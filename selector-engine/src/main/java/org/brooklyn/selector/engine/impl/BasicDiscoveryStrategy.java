package org.brooklyn.selector.engine.impl;

import org.brooklyn.selector.engine.DiscoveryStrategy;
import org.brooklyn.selector.integration.service.DiscoveryService;
import org.brooklyn.selector.integration.service.ParameterMapper;
import org.brooklyn.selector.model.User;
import org.brooklyn.selector.model.engine.Context;
import org.brooklyn.selector.model.engine.Response;
import org.brooklyn.selector.model.integration.Result;
import org.brooklyn.selector.model.understanding.Understanding;
import org.brooklyn.selector.understanding.UnderstandingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class BasicDiscoveryStrategy implements DiscoveryStrategy {

    private final UnderstandingService understandingService;
    private final DiscoveryService discoveryService;
    private final ParameterMapper parameterMapper;
    private final Response response;

    @Autowired
    public BasicDiscoveryStrategy(UnderstandingService understandingService, DiscoveryService discoveryService,
                                  ParameterMapper parameterMapper, Response response) {
        this.understandingService = understandingService;
        this.discoveryService = discoveryService;
        this.parameterMapper = parameterMapper;
        this.response = response;
    }

    @Override
    public boolean evaluate(Context context, User user) {
        return true;
    }

    @Override
    public Response process(String request) throws IOException {
        Understanding understanding = understandingService.understand(request);
        Result result = discoveryService.discover(parameterMapper.convertEntitiesToParameters(understanding.getEntities()));
        response.setResults(result.getItems());
        return response;
    }

}
