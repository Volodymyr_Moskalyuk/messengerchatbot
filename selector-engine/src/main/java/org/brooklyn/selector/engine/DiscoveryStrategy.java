package org.brooklyn.selector.engine;

import org.brooklyn.selector.model.User;
import org.brooklyn.selector.model.engine.Context;
import org.brooklyn.selector.model.engine.Request;
import org.brooklyn.selector.model.engine.Response;
import org.brooklyn.selector.model.integration.Result;

import java.io.IOException;

public interface DiscoveryStrategy {

	//boolean evaluate(Context context, User user, Result result);
	boolean evaluate(Context context, User user);

	Response process(String request) throws IOException;
	
}
