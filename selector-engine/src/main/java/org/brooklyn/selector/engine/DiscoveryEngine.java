package org.brooklyn.selector.engine;

import java.io.IOException;
import java.util.List;

import org.brooklyn.selector.engine.impl.BasicDiscoveryStrategy;
import org.brooklyn.selector.model.engine.Request;
import org.brooklyn.selector.model.engine.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DiscoveryEngine {

   //private List<DiscoveryStrategy> strategies;

    @Autowired
    private DiscoveryStrategy discoveryStrategy;

    public Response process(String request) throws IOException {
//        for (DiscoveryStrategy discoveryStrategy : strategies) {
//            if (discoveryStrategy.evaluate(request.getUser())){
                return discoveryStrategy.process(request);
//            }
//        }
//
//
//        return notHandled();
    }

    private Response notHandled() {
        return new Response("Sorry, bad query", null);
    }
}

