package org.brooklyn.selector.integration.service;

import org.brooklyn.selector.model.integration.Parameter;
import org.brooklyn.selector.model.integration.additional.city.City;
import org.brooklyn.selector.model.integration.additional.hotel.Hotel;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface HotelData {

    List<Parameter> getAllParameters();

     Hotel getAllHotels(String city) throws IOException;

    Map<String, String> getTypeHotels();

    List<City> getAllCities();

}
