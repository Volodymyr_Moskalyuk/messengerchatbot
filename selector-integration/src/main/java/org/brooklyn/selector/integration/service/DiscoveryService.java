package org.brooklyn.selector.integration.service;

import java.io.IOException;
import java.util.List;

import org.brooklyn.selector.model.integration.Parameter;
import org.brooklyn.selector.model.integration.Result;

public interface DiscoveryService {

	Result discover(List<Parameter> input) throws IOException;
}
