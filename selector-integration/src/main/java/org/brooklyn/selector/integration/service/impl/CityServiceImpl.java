package org.brooklyn.selector.integration.service.impl;

import org.brooklyn.selector.integration.service.CityService;
import org.brooklyn.selector.integration.service.HotelData;
import org.brooklyn.selector.model.integration.additional.city.City;
import org.brooklyn.selector.model.integration.additional.city.Language;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

@Service
public class CityServiceImpl implements CityService {
    private static final String FILE_PATH = "selector-integration/city.txt";

    @Autowired
    private HotelData hotelData;

    @Override
    public void writeCitiesToFile() {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILE_PATH))) {
            for (City city : hotelData.getAllCities()) {
                addCityToFile(bw, city);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getValueByKey(String idCity) throws IOException {
        return getCitiesFromFile().get(idCity);
    }

    @Override
    public String getKeyByValue(String cityName) throws IOException {
        return String.valueOf(getCitiesFromFile().entrySet().stream()
                .filter(entry -> entry.getValue().equalsIgnoreCase(cityName))
                .findFirst().get().getKey());
    }

    private void addCityToFile(BufferedWriter bw, City city) throws IOException {
        for (Language language : city.getName()) {
            if (language != null) {
                bw.write(city.getId() + " " + language.getName().get(0) + "\n");
            }
        }
    }

    private Map<String, String> getCitiesFromFile() throws IOException {
        Map<String, String> map = new HashMap<>();
        BufferedReader reader = new BufferedReader(new FileReader(FILE_PATH));
        String line;

        while ((line = reader.readLine()) != null) {
            Stream.of(line)
                    .map(string -> string.split(" ", 2))
                    .filter(strings -> strings.length >= 2)
                    .forEach(str -> map.put(str[0], str[1]));
        }

        reader.close();
        return map;
    }
}
