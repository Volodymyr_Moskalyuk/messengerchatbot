package org.brooklyn.selector.integration.service.impl;


import org.brooklyn.selector.integration.service.CityService;
import org.brooklyn.selector.integration.service.HotelData;
import org.brooklyn.selector.model.integration.Parameter;
import org.brooklyn.selector.model.integration.additional.city.City;
import org.brooklyn.selector.model.integration.additional.hotel.Hotel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class HotelDataImpl implements HotelData {
    private static final String URL_PARAMETERS = "http://engine.hotellook.com/api/v2/static/amenities/en.json?token=9370c9ccd39aee4ab312ced64432d066";
    private static final String URL_HOTELS = "http://engine.hotellook.com/api/v2/static/hotels.json?locationId=";
    private static final String URL_HOTELS_TYPE = "http://engine.hotellook.com/api/v2/static/hotelTypes.json?language=en&token=9370c9ccd39aee4ab312ced64432d066";
    private static final String URL_CITY = "http://engine.hotellook.com/api/v2/static/locations.json?token=9370c9ccd39aee4ab312ced64432d066";
    private static final String TOKEN = "&token=9370c9ccd39aee4ab312ced64432d066";

    private final RestTemplate restTemplate;
    private final CityService cityService;

    @Autowired
    public HotelDataImpl(RestTemplate restTemplate, CityService cityService) {
        this.restTemplate = restTemplate;
        this.cityService = cityService;
    }

    @Override
    public List<Parameter> getAllParameters() {
        ResponseEntity<Parameter[]> response = restTemplate.getForEntity(URL_PARAMETERS, Parameter[].class);
        return Arrays.asList(response.getBody());
    }

    @Override
    public Hotel getAllHotels(String city) throws IOException {
        ResponseEntity<Hotel> response = restTemplate.getForEntity(URL_HOTELS + cityService.getKeyByValue(city)
                + TOKEN, Hotel.class);
        return response.getBody();
    }

    @Override
    public Map<String, String> getTypeHotels() {
        return restTemplate.getForObject(URL_HOTELS_TYPE, HashMap.class);
    }

    @Override
    public List<City> getAllCities() {
        ResponseEntity<City[]> response = restTemplate.getForEntity(URL_CITY, City[].class);
        return Arrays.asList(response.getBody());
    }
}
