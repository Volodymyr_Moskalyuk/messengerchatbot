package org.brooklyn.selector.integration.service;

import org.brooklyn.selector.model.integration.Parameter;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface ParameterMap {

    Map< Parameter, String> getMapOfParameters();

}
