package org.brooklyn.selector.integration.service.impl;

import org.brooklyn.selector.integration.service.ParameterMap;
import org.brooklyn.selector.integration.service.ParameterMapper;
import org.brooklyn.selector.model.integration.Parameter;
import org.brooklyn.selector.model.understanding.Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class ParameterMapperImpl implements ParameterMapper {

    private final String LOCATION_TYPE = "location";
    private final ParameterMap parameterMap;
    private final Parameter parameterCity;

    @Autowired
    public ParameterMapperImpl(ParameterMap parameterMap, Parameter parameterCity) {
        this.parameterMap = parameterMap;
        this.parameterCity = parameterCity;
    }

    @Override
    public List<Parameter> convertEntitiesToParameters(List<Entity> token) {
        List<Parameter> inputParameters = new ArrayList<>();
        HashMap<Parameter, String> mapOfParameters = new HashMap<>(parameterMap.getMapOfParameters());

        addCityParameterToList(token, inputParameters);
        token.forEach(entity -> addParameterToList(inputParameters, mapOfParameters, entity));

        return inputParameters;
    }

    private void addParameterToList(List<Parameter> inputParameters, HashMap<Parameter, String> map, Entity entity) {
        map.entrySet().stream()
                .filter(entry -> entry.getValue().equalsIgnoreCase(entity.getName()))
                .forEach(entry -> inputParameters.add(entry.getKey()));
    }

    private void addCityParameterToList(List<Entity> token, List<Parameter> inputParameters) {
        token.stream()
                .filter(entity -> entity.getType().equalsIgnoreCase(LOCATION_TYPE))
                .limit(1)
                .forEach(entity -> {
                    parameterCity.setId(0L);
                    parameterCity.setName(entity.getName());
                });
        inputParameters.add(parameterCity);
    }
}
