package org.brooklyn.selector.integration.service.impl;

import org.brooklyn.selector.integration.service.ParameterMap;
import org.brooklyn.selector.model.integration.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

@Service
public class ParameterMapImpl implements ParameterMap {

    private static final String FILE_PATH = "selector-integration/ParametersMap";

    private final Parameter parameter;

    @Autowired
    public ParameterMapImpl(Parameter parameter) {
        this.parameter = parameter;
    }


    @Override
    public Map<Parameter, String> getMapOfParameters() {
        Map<Parameter, String> parameterMap = new HashMap<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(FILE_PATH))) {
            addParameterToMap(parameterMap, reader);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return parameterMap;
    }

    private void addParameterToMap(Map<Parameter, String> parameterMap, BufferedReader reader) throws IOException {
        String line;
        while ((line = reader.readLine()) != null) {
            Stream.of(line)
                    .map(str -> str.split(":", 2))
                    .filter(strings -> strings.length >= 2)
                    .forEach(strings -> parameterMap.put(convertStringToParameter(strings[0]), strings[1]));
        }
    }

    private Parameter convertStringToParameter(String inputParameter) {
        List<String> list = Arrays.asList(inputParameter.split("=", 2));
        parameter.setId(Long.valueOf(list.get(0)));
        parameter.setName(list.get(1));
        return parameter;
    }
}
