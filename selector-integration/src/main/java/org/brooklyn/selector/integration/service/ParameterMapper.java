package org.brooklyn.selector.integration.service;

import org.brooklyn.selector.model.integration.Parameter;
import org.brooklyn.selector.model.understanding.Entity;

import java.io.IOException;
import java.util.List;

public interface ParameterMapper {

	List<Parameter> convertEntitiesToParameters(List<Entity> token) throws IOException;
	
}
