package org.brooklyn.selector.integration.service;

import java.io.IOException;

public interface CityService {

    void writeCitiesToFile();

    String getValueByKey(String idCity) throws IOException;

    String getKeyByValue(String cityName) throws IOException;

}
