package org.brooklyn.selector.integration.service.impl;

import org.brooklyn.selector.integration.service.DiscoveryService;
import org.brooklyn.selector.integration.service.HotelData;
import org.brooklyn.selector.model.integration.Item;
import org.brooklyn.selector.model.integration.Parameter;
import org.brooklyn.selector.model.integration.Result;
import org.brooklyn.selector.model.integration.additional.hotel.Hotel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DiscoveryServiceImpl implements DiscoveryService {

    private static final List<Long> ALL_POOL_PARAMETERS = Arrays.asList(87L, 68L, 37L, 38L, 86L);
    private static final List<Long> ALL_PARKING_PARAMETERS = Arrays.asList(54L, 56L);
    private static final List<Long> ALL_ROOM_SV_PARAMETERS = Arrays.asList(23L, 25L);
    private static final List<Long> ALL_PARAMETERS = Arrays.asList(87L, 68L, 37L, 38L, 86L, 54L, 56L, 23L, 25L);

    private final Result result;
    private final HotelData hotelData;

    @Autowired
    public DiscoveryServiceImpl(Result result, HotelData hotelData) {
        this.result = result;
        this.hotelData = hotelData;
    }

    @Override
    public Result discover(List<Parameter> input) throws IOException {

        String city = input.get(0).getName();

        List<Long> listOfParameters = getListOfParameters(input);
        Hotel allHotels = hotelData.getAllHotels(city);

        List<Item> resultSearch = allHotels.getHotels().stream()
                .filter(item -> checkParameters(item, getInterimList(listOfParameters)))
                .collect(Collectors.toList());

        result.setItem(resultSearch);
        return result;
    }

    private List<Long> getListOfParameters(List<Parameter> input) {
        return input.stream()
                .skip(1)
                .map(Parameter::getId)
                .collect(Collectors.toList());
    }

    private List<Long> getInterimList(List<Long> listOfParameters) {
        return new ArrayList<>(listOfParameters);
    }

    private boolean checkSize(Item item, List<Long> interimList) {
        return !item.getFacilities().isEmpty() && (item.getFacilities().size() >= interimList.size());
    }

    private boolean checkParameters(Item item, List<Long> interimList) {
        boolean isEmptyInterimList = false;
        if (checkSize(item, interimList)) {
            interimList.removeAll(item.getFacilities());
            isEmptyInterimList = isEmptyInterimList(interimList);
        }
        return isEmptyInterimList;
    }

    private boolean isEmptyInterimList(List<Long> interimList) {
        boolean isContainAllParkingParameters = !interimList.containsAll(ALL_PARKING_PARAMETERS);
        boolean isContainAllAddPoolParameters = !interimList.containsAll(ALL_POOL_PARAMETERS);
        boolean isContainAllRoomServiceParameters = !interimList.containsAll(ALL_ROOM_SV_PARAMETERS);
        boolean isEmpty = false;

        if (isContainAllParkingParameters && isContainAllAddPoolParameters && isContainAllRoomServiceParameters) {
            interimList.removeAll(ALL_PARAMETERS);
            isEmpty = interimList.isEmpty();
        }
        return isEmpty;
    }
}
