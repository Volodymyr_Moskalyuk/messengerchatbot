package org.brooklyn.selector.model.knowledge.feature;


import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.List;

@NodeEntity
public class Decision {

    @GraphId
    private Long id;
    private String nameOfHotel;

    @Relationship(type = "FEATURE")
    private List<Feature> features;

    @Relationship(type = "FILTER")
    private List<Filter> filters;

    public Decision() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameOfHotel() {
        return nameOfHotel;
    }

    public void setNameOfHotel(String nameOfHotel) {
        this.nameOfHotel = nameOfHotel;
    }

    public List<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

    public List<Filter> getFilters() {
        return filters;
    }

    public void setFilters(List<Filter> filters) {
        this.filters = filters;
    }

    @Override
    public String toString() {
        return "Decision{" +
                "id=" + id +
                ", nameOfHotel='" + nameOfHotel + '\'' +
                '}';
    }
}
