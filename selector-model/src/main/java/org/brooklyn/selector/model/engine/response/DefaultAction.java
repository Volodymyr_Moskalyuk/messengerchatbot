package org.brooklyn.selector.model.engine.response;

public class DefaultAction {
    private String webViewHeightRatio;
    private String fallbackUrl;
    private String type;
    private String url;
    private String messengerExtensions;

    public String getWebViewHeightRatio() {
        return webViewHeightRatio;
    }

    public void setWebViewHeightRatio(String webViewHeightRatio) {
        this.webViewHeightRatio = webViewHeightRatio;
    }

    public String getFallbackUrl() {
        return fallbackUrl;
    }

    public void setFallbackUrl(String fallbackUrl) {
        this.fallbackUrl = fallbackUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMessengerExtensions() {
        return messengerExtensions;
    }

    public void setMessengerExtensions(String messengerExtensions) {
        this.messengerExtensions = messengerExtensions;
    }

    @Override
    public String toString() {
        return "DefaultAction{" +
                "webviewHeightRatio='" + webViewHeightRatio + '\'' +
                ", fallbackUrl='" + fallbackUrl + '\'' +
                ", type='" + type + '\'' +
                ", url='" + url + '\'' +
                ", messengerExtensions='" + messengerExtensions + '\'' +
                '}';
    }
}
