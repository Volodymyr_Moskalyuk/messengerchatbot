package org.brooklyn.selector.model.engine.request;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.web.context.support.HttpRequestHandlerServlet;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestModel {
    private List<Entries> entry;

    public List<Entries> getEntry() {
        return entry;
    }

    public void setEntry(List<Entries> entry) {
        this.entry = entry;
    }

    @Override
    public String toString() {
        return "RequestModel{" +
                "entry=" + entry +
                '}';
    }
}
