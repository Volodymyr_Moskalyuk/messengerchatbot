package org.brooklyn.selector.model.knowledge.feature;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity
public class Attribute {

    @GraphId
    private Long id;
    private String nameOfAttribute;

    @Relationship(type = "ATTRIBUTE", direction = Relationship.INCOMING)
    private Feature feature;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameOfAttribute() {
        return nameOfAttribute;
    }

    public void setNameOfAttribute(String nameOfAttribute) {
        this.nameOfAttribute = nameOfAttribute;
    }

    public Feature getFeature() {
        return feature;
    }

    public void setFeature(Feature feature) {
        this.feature = feature;
    }

    @Override
    public String toString() {
        return "Attribute{" +
                "id=" + id +
                ", nameOfAttribute='" + nameOfAttribute + '\'' +
                '}';
    }
}
