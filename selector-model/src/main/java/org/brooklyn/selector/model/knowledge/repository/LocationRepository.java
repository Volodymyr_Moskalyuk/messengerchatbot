package org.brooklyn.selector.model.knowledge.repository;

import org.brooklyn.selector.model.knowledge.feature.Location;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRepository extends Neo4jRepository<Location, Long> {
}
