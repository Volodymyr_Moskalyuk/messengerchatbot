package org.brooklyn.selector.model.engine.response;

import java.util.List;



public class Payload {
    private String template_type;
    private List<Elements> elements;

    public String getTemplate_type ()
    {
        return template_type;
    }

    public void setTemplate_type (String template_type)
    {
        this.template_type = template_type;
    }

    public List<Elements> getElements ()
    {
        return elements;
    }

    public void setElements (List<Elements> elements)
    {
        this.elements = elements;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [template_type = "+template_type+", elements = "+elements+"]";
    }
}
