package org.brooklyn.selector.model.integration.additional.hotel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.brooklyn.selector.model.integration.Item;

import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Hotel {

    @JsonProperty("hotels")
    private List<Item> hotels;

    public List<Item> getHotels() {
        return hotels;
    }

    public void setHotels(List<Item> hotels) {
        this.hotels = hotels;
    }

    @Override
    public String toString() {
        return "hotels:" + hotels + "\n" +
                '}';
    }
}

