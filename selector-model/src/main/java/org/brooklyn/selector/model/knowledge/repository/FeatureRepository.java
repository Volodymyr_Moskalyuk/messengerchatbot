package org.brooklyn.selector.model.knowledge.repository;

import org.brooklyn.selector.model.knowledge.feature.Feature;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FeatureRepository extends Neo4jRepository<Feature, Long> {
}
