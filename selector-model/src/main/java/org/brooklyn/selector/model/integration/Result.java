package org.brooklyn.selector.model.integration;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Component
public class Result {

   private List<Item> item;

    public List<Item> getItems() {
        return item;
    }

    public void setItem(List<Item> item) {
        this.item = item;
    }

    @Override
    public String toString() {
        return "Result{" +
                "item=" + item +
                '}';
    }
}
