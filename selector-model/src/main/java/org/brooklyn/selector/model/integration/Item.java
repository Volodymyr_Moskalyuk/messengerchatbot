package org.brooklyn.selector.model.integration;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.brooklyn.selector.model.integration.additional.hotel.*;

import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Item {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("cityId")
    private Long cityId;
    @JsonProperty("stars")
    private Long stars;
    @JsonProperty("pricefrom")
    private Long pricefrom;
    @JsonProperty("rating")
    private Long rating;
    @JsonProperty("popularity")
    private Long popularity;
    @JsonProperty("propertyType")
    private Long propertyType;
    @JsonProperty("checkIn")
    private String checkIn;
    @JsonProperty("checkOut")
    private String checkOut;
    @JsonProperty("distance")
    private Long distance;
    @JsonProperty("photoCount")
    private Long photoCount;
    @JsonProperty("photos")
    private List<Photo> photos = null;
    @JsonProperty("yearOpened")
    private String yearOpened;
    @JsonProperty("yearRenovated")
    private String yearRenovated;
    @JsonProperty("cntRooms")
    private String cntRooms;
    @JsonProperty("cntSuites")
    private String cntSuites;
    @JsonProperty("cntFloors")
    private String cntFloors;
    @JsonProperty("facilities")
    private List<Long> facilities = null;
    @JsonProperty("shortFacilities")
    private List<String> shortFacilities = null;
    @JsonProperty("location")
    private Location location;
    @JsonProperty("name")
    private Name name;
    @JsonProperty("address")
    private Address address;
    @JsonProperty("link")
    private String link;

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("cityId")
    public Long getCityId() {
        return cityId;
    }

    @JsonProperty("cityId")
    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    @JsonProperty("stars")
    public Long getStars() {
        return stars;
    }

    @JsonProperty("stars")
    public void setStars(Long stars) {
        this.stars = stars;
    }

    @JsonProperty("pricefrom")
    public Long getPricefrom() {
        return pricefrom;
    }

    @JsonProperty("pricefrom")
    public void setPricefrom(Long pricefrom) {
        this.pricefrom = pricefrom;
    }

    @JsonProperty("rating")
    public Long getRating() {
        return rating;
    }

    @JsonProperty("rating")
    public void setRating(Long rating) {
        this.rating = rating;
    }

    @JsonProperty("popularity")
    public Long getPopularity() {
        return popularity;
    }

    @JsonProperty("popularity")
    public void setPopularity(Long popularity) {
        this.popularity = popularity;
    }

    @JsonProperty("propertyType")
    public Long getPropertyType() {
        return propertyType;
    }

    @JsonProperty("propertyType")
    public void setPropertyType(Long propertyType) {
        this.propertyType = propertyType;
    }

    @JsonProperty("checkIn")
    public String getCheckIn() {
        return checkIn;
    }

    @JsonProperty("checkIn")
    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }

    @JsonProperty("checkOut")
    public String getCheckOut() {
        return checkOut;
    }

    @JsonProperty("checkOut")
    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    @JsonProperty("distance")
    public Long getDistance() {
        return distance;
    }

    @JsonProperty("distance")
    public void setDistance(Long distance) {
        this.distance = distance;
    }

    @JsonProperty("photoCount")
    public Long getPhotoCount() {
        return photoCount;
    }

    @JsonProperty("photoCount")
    public void setPhotoCount(Long photoCount) {
        this.photoCount = photoCount;
    }

    @JsonProperty("photos")
    public List<Photo> getPhotos() {
        return photos;
    }

    @JsonProperty("photos")
    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    @JsonProperty("yearOpened")
    public Object getYearOpened() {
        return yearOpened;
    }

    @JsonProperty("yearOpened")
    public void setYearOpened(String yearOpened) {
        this.yearOpened = yearOpened;
    }

    @JsonProperty("yearRenovated")
    public String getYearRenovated() {
        return yearRenovated;
    }

    @JsonProperty("yearRenovated")
    public void setYearRenovated(String yearRenovated) {
        this.yearRenovated = yearRenovated;
    }

    @JsonProperty("cntRooms")
    public String getCntRooms() {
        return cntRooms;
    }

    @JsonProperty("cntRooms")
    public void setCntRooms(String cntRooms) {
        this.cntRooms = cntRooms;
    }

    @JsonProperty("cntSuites")
    public String getCntSuites() {
        return cntSuites;
    }

    @JsonProperty("cntSuites")
    public void setCntSuites(String cntSuites) {
        this.cntSuites = cntSuites;
    }

    @JsonProperty("cntFloors")
    public String getCntFloors() {
        return cntFloors;
    }

    @JsonProperty("cntFloors")
    public void setCntFloors(String cntFloors) {
        this.cntFloors = cntFloors;
    }

    @JsonProperty("facilities")
    public List<Long> getFacilities() {
        return facilities;
    }

    @JsonProperty("facilities")
    public void setFacilities(List<Long> facilities) {
        this.facilities = facilities;
    }

    @JsonProperty("shortFacilities")
    public List<String> getShortFacilities() {
        return shortFacilities;
    }

    @JsonProperty("shortFacilities")
    public void setShortFacilities(List<String> shortFacilities) {
        this.shortFacilities = shortFacilities;
    }

    @JsonProperty("location")
    public Location getLocation() {
        return location;
    }

    @JsonProperty("location")
    public void setLocation(Location location) {
        this.location = location;
    }

    @JsonProperty("name")
    public Name getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(Name name) {
        this.name = name;
    }

    @JsonProperty("address")
    public Address getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(Address address) {
        this.address = address;
    }

    @JsonProperty("link")
    public String getLink() {
        return link;
    }

    @JsonProperty("link")
    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String toString() {
        return "Hotel{" +
                "id=" + id +
                ", cityId=" + cityId +
//                ", stars=" + stars +
//                ", pricefrom=" + pricefrom +
//                ", rating=" + rating +
//                ", popularity=" + popularity +
//                ", propertyType=" + propertyType +
//                ", checkIn='" + checkIn + '\'' +
//                ", checkOut='" + checkOut + '\'' +
//                ", distance=" + distance +
//                ", photoCount=" + photoCount +
//                ", photos=" + photos +
//                ", yearOpened='" + yearOpened + '\'' +
//                ", yearRenovated='" + yearRenovated + '\'' +
//                ", cntRooms='" + cntRooms + '\'' +
//                ", cntSuites='" + cntSuites + '\'' +
//                ", cntFloors='" + cntFloors + '\'' +
                ", facilities=" + facilities +
//                ", shortFacilities=" + shortFacilities +
//                ", location=" + location +
                ", name=" + name +
                ", address=" + address +
               // ", link='" + link + '\'' +
                '}';
    }

}
