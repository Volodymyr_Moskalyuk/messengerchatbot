package org.brooklyn.selector.model.knowledge.feature;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity
public class Synonymn {

    @GraphId
    private Long id;
    private String nameOfSynonymn;

    @Relationship(type = "SYNONYMNS", direction = Relationship.INCOMING)
    private Feature feature;

    @Relationship(type = "SYNONYMNS", direction = Relationship.INCOMING)
    private Filter filter;

    public Synonymn() {
    }

    public Synonymn(String nameOfSynonymn) {
        this.nameOfSynonymn = nameOfSynonymn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameOfSynonymn() {
        return nameOfSynonymn;
    }

    public void setNameOfSynonymn(String nameOfSynonymn) {
        this.nameOfSynonymn = nameOfSynonymn;
    }

    public Feature getFeature() {
        return feature;
    }

    public void setFeature(Feature feature) {
        this.feature = feature;
    }

    public Filter getFilter() {
        return filter;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }
}
