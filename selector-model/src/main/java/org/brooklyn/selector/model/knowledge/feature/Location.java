package org.brooklyn.selector.model.knowledge.feature;

import org.brooklyn.selector.model.knowledge.feature.Filter;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.List;

@NodeEntity
public class Location {

    @GraphId
    private Long id;
    private String location;

    @Relationship(type = "FILTER", direction = Relationship.INCOMING)
    private List<Filter> filters;

    public Location() {
    }

    public Location(String location) {
        this.location = location;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<Filter> getFilters() {
        return filters;
    }

    public void setFilters(List<Filter> filters) {
        this.filters = filters;
    }

    @Override
    public String toString() {
        return "Location{" +
                "id=" + id +
                ", location='" + location + '\'' +
                '}';
    }
}
