package org.brooklyn.selector.model.integration.additional.hotel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HotelType {


    private Map<String,String> typeHotel;

    public Map<String, String> getTypeHotel() {
        return typeHotel;
    }

    public void setTypeHotel(Map<String, String> typeHotel) {
        this.typeHotel = typeHotel;
    }

    @Override
    public String toString() {
        return "HotelType{" +
                "typeHotel=" + typeHotel +
                '}';
    }

}
