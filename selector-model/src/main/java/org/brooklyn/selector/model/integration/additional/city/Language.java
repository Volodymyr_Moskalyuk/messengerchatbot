package org.brooklyn.selector.model.integration.additional.city;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Language {

    @JsonProperty("EN")
    private List<CityName> name;

    public List<CityName> getName() {
        return name;
    }

    public void setName(List<CityName> name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return " " + name;
    }
}
