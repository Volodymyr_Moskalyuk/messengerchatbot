package org.brooklyn.selector.model.understanding;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor

public class Understanding {

    private Intent intent;
    @Getter
    @Setter
    private List<Entity> entities;
    private List<Predicate> predicates;

    public Understanding() {
        intent = null;
        entities = null;
        predicates = null;
    }
}
