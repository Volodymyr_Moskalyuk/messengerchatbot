package org.brooklyn.selector.model.engine.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Attachment {
    @JsonProperty("payload")
    private Payload payload;
    @JsonProperty("type")
    private String type;

    public Payload getPayload ()
    {
        return payload;
    }

    public void setPayload (Payload payload)
    {
        this.payload = payload;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [payload = "+payload+", type = "+type+"]";
    }
}
