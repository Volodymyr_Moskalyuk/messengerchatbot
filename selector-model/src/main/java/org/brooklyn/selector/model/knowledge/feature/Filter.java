package org.brooklyn.selector.model.knowledge.feature;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.List;

@NodeEntity
public class Filter {

    @GraphId
    private Long id;

    @Relationship(type = "SYNONYMNS")
    private List<Synonymn> synonymns;

    @Relationship(type = "FILTER", direction = Relationship.INCOMING)
    private Decision decision;

    @Relationship(type = "FILTER")
    private Location location;
    @Relationship(type = "FILTER")
    private RentDate rentDate;

    @Relationship(type = "FILTER")
    private Category category;

    @Relationship(type = "FILTER")
    private VisitorsCount visitorsCount;

    @Relationship(type = "FILTER")
    private Meal meal;

    public Filter() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Synonymn> getSynonymns() {
        return synonymns;
    }

    public void setSynonymns(List<Synonymn> synonymns) {
        this.synonymns = synonymns;
    }

    public Decision getDecision() {
        return decision;
    }

    public void setDecision(Decision decision) {
        this.decision = decision;
    }
}
