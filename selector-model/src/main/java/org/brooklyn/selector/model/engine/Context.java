package org.brooklyn.selector.model.engine;

import java.util.ArrayList;

public class Context {
	
	private ArrayList<String> parameters;
	private boolean isMessage;
	
	public Context() {
		this.parameters = null;
		this.isMessage = false;
	}
	
	public Context(ArrayList<String> parameters, boolean isMessage) {
		this.parameters = parameters;
		this.isMessage = isMessage;
	}
	
	public ArrayList<String> getParameters() {
		return parameters;
	}
	
	public void setParameters(ArrayList<String> parameters) {
		this.parameters = parameters;
	}
	
	public boolean isMessage() {
		return isMessage;
	}
	
	public void setMessage(boolean message) {
		isMessage = message;
	}
}
