package org.brooklyn.selector.model.engine;

import lombok.Data;
import org.brooklyn.selector.model.User;

@Data
public class Request {
	
	private String id;
	
	private String answer;
	
	private Context context;
	
	private User user;
	
	public Request() {
		id = null;
		answer = null;
		context = null;
		user = null;
	}
	
	public Request(String id, String answer, Context context, User user) {
		this.id = id;
		this.answer = answer;
		this.context = context;
		this.user = user;
	}
	
	public Context getContext() {
		return context;
	}
	
	public void setContext(Context context) {
		this.context = context;
	}
	
	public String getAnswer() {
		return answer;
	}
	
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
}
