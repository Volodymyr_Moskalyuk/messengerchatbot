package org.brooklyn.selector.model;

public class User {
	
	private String id;
	
	public User() {
		this.id = null;
	}
	
	public User(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
}
