package org.brooklyn.selector.model.understanding;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Entity {
    private int start;
    private int end;
    private String name;
    private String type;

    public Entity(String name, String type) {
        this.name = name;
        this.type = type;
    }

    @Override
    public String toString() {
        return "Entity{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
