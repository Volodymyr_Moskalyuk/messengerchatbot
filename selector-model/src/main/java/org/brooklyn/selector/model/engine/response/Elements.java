package org.brooklyn.selector.model.engine.response;


public class Elements {
    private String title;
    private String image_url;
    private String subtitle;
    private DefaultAction defaultAction;

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getImage_url ()
    {
        return image_url;
    }

    public void setImage_url (String image_url)
    {
        this.image_url = image_url;
    }

    public String getSubtitle ()
    {
        return subtitle;
    }

    public void setSubtitle (String subtitle)
    {
        this.subtitle = subtitle;
    }

    public DefaultAction getDefaultAction ()
    {
        return defaultAction;
    }

    public void setDefaultAction (DefaultAction default_action)
    {
        this.defaultAction = default_action;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [title = "+title+", image_url = "+image_url+", subtitle = "+subtitle+", default_action = "+defaultAction+"]";
    }
}
