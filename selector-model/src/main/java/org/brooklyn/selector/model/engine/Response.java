package org.brooklyn.selector.model.engine;

import lombok.Getter;
import lombok.Setter;
import org.brooklyn.selector.model.integration.Item;
import org.springframework.stereotype.Component;

import java.util.List;

@Getter
@Setter
@Component
public class Response  {
	
	private String message;
	private List<Item> results;
	
	public Response() {
	}

	public Response(String message) {
		this.message = message;
	}

	public Response(String message, List<Item> results) {
		this.message = message;
		this.results = results;
	}
}
