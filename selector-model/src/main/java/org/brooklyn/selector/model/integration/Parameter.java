package org.brooklyn.selector.model.integration;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Parameter {

    private Long id;
    private String name;

    public Parameter() {
    }

    @Override
    public String toString() {
        return "Parameter{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
