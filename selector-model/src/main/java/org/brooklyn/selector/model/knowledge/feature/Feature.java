package org.brooklyn.selector.model.knowledge.feature;

import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.springframework.data.annotation.Id;

import javax.persistence.GeneratedValue;
import java.util.List;

@NodeEntity
public class Feature {

    @Id
    @GeneratedValue
    private Long id;
    private String nameOfFeature;

    @Relationship(type = "FEATURE", direction = Relationship.INCOMING)
    private Decision decision;

    @Relationship(type = "ATTRIBUTE")
    private List<Attribute> attributes;

    @Relationship(type = "SYNONYMNS")
    private List<Synonymn> synonymns;

    public Feature() {
    }



    public Feature(String nameOfFeature) {
        this.nameOfFeature = nameOfFeature;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameOfFeature() {
        return nameOfFeature;
    }

    public void setNameOfFeature(String nameOfFeature) {
        this.nameOfFeature = nameOfFeature;
    }

    public Decision getDecision() {
        return decision;
    }

    public void setDecision(Decision decision) {
        this.decision = decision;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    public List<Synonymn> getSynonymns() {
        return synonymns;
    }

    public void setSynonymns(List<Synonymn> synonymns) {
        this.synonymns = synonymns;
    }

    public int getSize(){
        return nameOfFeature.length();
    }

    @Override
    public String toString() {
        return "Feature{" +
                "id=" + id +
                ", nameOfFeature='" + nameOfFeature + '\'' +
                '}';
    }
}
