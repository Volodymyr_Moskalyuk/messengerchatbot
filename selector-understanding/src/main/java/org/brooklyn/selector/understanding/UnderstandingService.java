package org.brooklyn.selector.understanding;

import org.brooklyn.selector.model.understanding.Understanding;

public interface UnderstandingService {
    Understanding understand(String query);
}
