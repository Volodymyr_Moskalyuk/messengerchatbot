package org.brooklyn.selector.understanding;

import org.brooklyn.selector.model.understanding.Entity;
import org.brooklyn.selector.model.understanding.Intent;
import org.brooklyn.selector.model.understanding.Predicate;
import org.brooklyn.selector.model.understanding.Understanding;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UnderstandingServiceImp implements UnderstandingService {

    private final EntityFinder entityFinder;

    @Autowired
    public UnderstandingServiceImp(EntityFinder entityFinder) {
        this.entityFinder = entityFinder;
    }

    @Override
    public Understanding understand(String query) {

        Intent intent = null;

        List<Predicate> predicates = null;

        List<Entity> entities = entityFinder.generateEntities(query);

        return new Understanding(intent, entities, predicates);
    }
}
