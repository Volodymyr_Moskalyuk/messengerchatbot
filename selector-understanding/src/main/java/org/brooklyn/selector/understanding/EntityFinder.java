package org.brooklyn.selector.understanding;

import org.brooklyn.selector.knowledge.service.KnowledgeService;
import org.brooklyn.selector.model.knowledge.feature.Feature;
import org.brooklyn.selector.model.knowledge.feature.Location;
import org.brooklyn.selector.model.understanding.Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

//TODO ignore case when contains (check bug with upper case)
@Component
public class EntityFinder {

    private final KnowledgeService knowledgeService;

    @Autowired
    public EntityFinder(KnowledgeService knowledgeService) {
        this.knowledgeService = knowledgeService;
    }

    List<Entity> generateEntities(String inputText) {


        List<Location> locations = knowledgeService.getAllLocation();
        List<Feature> features = knowledgeService.getAllFeatures();

        List<Entity> entities = new ArrayList<>();

        for (Feature feature : features) {
            if (inputText.toLowerCase().contains(feature.getNameOfFeature().toLowerCase())) {
                System.out.println("EntityFinder: generateEntities: feature: " + feature.getNameOfFeature());
                entities.add(new Entity(feature.getNameOfFeature(), "feature"));
            }
        }

        for (Location location : locations) {
            if (inputText.toLowerCase().contains(location.getLocation().toLowerCase())) {
                System.out.println("EntityFinder: generateEntities: location: " + location.getLocation());
                entities.add(new Entity(location.getLocation(), "location"));
            }
        }

        return entities;
    }

}

