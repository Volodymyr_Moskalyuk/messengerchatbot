package org.brooklyn.selector.knowledge.serviceImpl;

import org.brooklyn.selector.knowledge.service.KnowledgeService;
//import org.brooklyn.selector.model.GeneratingDB;
import org.brooklyn.selector.model.knowledge.feature.Decision;
import org.brooklyn.selector.model.knowledge.feature.Feature;
import org.brooklyn.selector.model.knowledge.feature.Filter;
import org.brooklyn.selector.model.knowledge.feature.Synonymn;
import org.brooklyn.selector.model.knowledge.feature.Location;
import org.brooklyn.selector.model.knowledge.repository.FeatureRepository;
import org.brooklyn.selector.model.knowledge.repository.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class KnowledgeServiceImp implements KnowledgeService {

    @Autowired
    private FeatureRepository featureRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Override
    public Set<Decision> getAllDecisions() {
        return null;
    }

    @Override
    public Set<Synonymn> getSynonynms(Long id) {
        return null;
    }

    @Override
    @Transactional
    public List<Feature> getAllFeatures() {
        List<Feature> listOfFeatures = (ArrayList<Feature>) featureRepository.findAll();
        return listOfFeatures;
    }

    @Override
    public Set<Filter> getAllFilters() {
        return null;
    }

    @Override
    public List<Location> getAllLocation() {
        List<Location> listOfLocation = (ArrayList<Location>) locationRepository.findAll();
        return listOfLocation;
    }


}
