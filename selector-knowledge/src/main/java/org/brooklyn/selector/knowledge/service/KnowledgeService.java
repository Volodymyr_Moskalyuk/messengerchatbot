package org.brooklyn.selector.knowledge.service;

import java.util.List;
import java.util.Set;

import org.brooklyn.selector.model.knowledge.feature.Decision;
import org.brooklyn.selector.model.knowledge.feature.Feature;
import org.brooklyn.selector.model.knowledge.feature.Filter;
import org.brooklyn.selector.model.knowledge.feature.Synonymn;
import org.brooklyn.selector.model.knowledge.feature.Location;

public interface KnowledgeService{
	
	Set<Decision> getAllDecisions();
	
	Set<Synonymn> getSynonynms(Long id);

	List<Feature> getAllFeatures();

	Set<Filter> getAllFilters();

	List<Location> getAllLocation();

}
