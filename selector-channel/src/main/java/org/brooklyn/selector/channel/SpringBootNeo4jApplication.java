package org.brooklyn.selector.channel;

import org.brooklyn.selector.model.knowledge.feature.Feature;
import org.brooklyn.selector.model.knowledge.feature.Location;
import org.brooklyn.selector.model.knowledge.repository.FeatureRepository;
import org.brooklyn.selector.model.knowledge.repository.LocationRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;

@SpringBootApplication(scanBasePackages = {"org.brooklyn.selector.integration", "org.brooklyn.selector.understanding",
        "org.brooklyn.selector.knowledge.serviceImpl", "org.brooklyn.selector.model.knowledge.repository",
        "org.brooklyn.selector.model","org.brooklyn.selector.engine","org.brooklyn.selector.channel" })
@EnableNeo4jRepositories(basePackages = "org.brooklyn.selector.model.knowledge.repository")
public class SpringBootNeo4jApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootNeo4jApplication.class, args);
    }
    @Bean
    CommandLineRunner demo(FeatureRepository featureRepository, LocationRepository locationRepository) {
        return args -> {

            System.out.println("-----------------------------------------");
            featureRepository.deleteAll();
            locationRepository.deleteAll();

            String[] listFeature = {"Parking", "Room Bar", "Air Conditioning", "Pets Allowed", "Room Dinning",
                    "Transfer", "Tennis Court", "Mail", "Car Rent", "SPA", "Pool", "Children Zone", "24 Reception",
                    "Kitchen", "Coffee Market", "Wake up Service", "Conference Room", "Breakfast", "TV", "Restaurant",
                    "Drier", "Room Service", "Non Smoking", "Fitness"};

            String[] listLocation = {"Lviv", "Kiev", "Madrid", "Rome", "Prague", "Dnipro", "London",
                    "Paris", "Lisbon", "Barcelona", "Amsterdam", "Miami", "Dubai", "Tokyo"};

            for (String f : listFeature) {
                Feature feature = new Feature(f);
                featureRepository.save(feature);
            }

            for (String l : listLocation) {
                Location location = new Location(l);
                locationRepository.save(location);
            }

            System.out.println(featureRepository.findAll());
            System.out.println("---------------------------");
            System.out.println(locationRepository.findAll());
        };
    }

}
