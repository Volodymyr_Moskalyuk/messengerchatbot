package org.brooklyn.selector.channel;


import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.brooklyn.selector.engine.DiscoveryStrategy;
import org.brooklyn.selector.model.engine.request.RequestModel;
import org.brooklyn.selector.model.integration.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

// GET request = FB webhook checks ngrok server with verifyToken
// POST request = all other FB webhooks (message, error, response)

@RestController
public class MessengerWebhook {

    private static final String verifyToken = "yes777";
    private static final String ACCESS_TOKEN = "https://graph.facebook.com/v2.6/me/messages?access_token=EAAdiP4xyXqoBAHFyyhZAzHCJ5uEcRbZBbX0CHsbGNBperLCVBAUBN7zvoGP1H1DCGnvz0fv9ot2d97pGLd4tTk5XUHWXe2yWQqowHISnz6pH3q41FHZCfPkgZCeOacshF8m1uvVseTivCl09Qs9I7YC8dEe4B6ljf1D9bUTLN6yiByxeCjer";

    private final DiscoveryStrategy discoveryStrategy;

    private final ResponseTransformer responseTransformer;

    private final Result result;

    @Autowired
    public MessengerWebhook(DiscoveryStrategy discoveryStrategy, ResponseTransformer responseTransformer, Result result) {
        this.discoveryStrategy = discoveryStrategy;
        this.responseTransformer = responseTransformer;
        this.result = result;
    }


    @RequestMapping(value = "/webhook", method = RequestMethod.POST)
    public @ResponseBody
    Response receiveWebhook(@RequestBody RequestModel requestModel) throws IOException {
        String textMsg = requestModel.getEntry().get(0).getMessaging().get(0).getMessage().getText();
        String recipientId = requestModel.getEntry().get(0).getMessaging().get(0).getSender().getId();
        result.setItem(discoveryStrategy.process(textMsg).getResults());
        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/json");
        okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType, responseTransformer.transformResponse(result, recipientId));
        Request request = new Request.Builder()
                .url(ACCESS_TOKEN)
                .post(body)
                .addHeader("content-type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();
        return client.newCall(request).execute();
    }


    @RequestMapping(value = "/webhook", method = RequestMethod.GET)
    public String checkToken(@RequestParam(value = "hub.challenge") String validationKey,
                             @RequestParam(value = "hub.verify_token") String verifyToken) {

        if (verifyToken.equals(this.verifyToken)) {
            return validationKey;
        } else {
            return "Bad verify token";
        }

    }

}
