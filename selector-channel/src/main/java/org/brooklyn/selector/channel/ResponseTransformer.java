package org.brooklyn.selector.channel;

import org.brooklyn.selector.model.engine.Response;
import org.brooklyn.selector.model.integration.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;


@Service
@Component
public class ResponseTransformer {

    // imageURL and clickURL must be whitelisted
    // title = hotel name, subtitle = additional info
    // responseSize = hotels in carousel (max = 10)
    // responseBody = body for POST request

    private String responseBody;

    @Autowired
    private Response response;


    public String transformResponse(Result result, String recipientId) {
        responseBody = "{\n" +
                "  \"recipient\":{\n" +
                "    \"id\":" + recipientId + "\n" +
                "  },\n" +
                "  \"message\":{\n" +
                "    \"attachment\":{\n" +
                "      \"type\":\"template\",\n" +
                "      \"payload\":{\n" +
                "        \"template_type\":\"generic\",\n" +
                "        \"elements\":[\n";


        for (int i = 0; i < result.getItems().size() && i < 10; i++) {
            responseBody = responseBody +
                    "{\n" +
                    "            \"title\":\"" + result.getItems().get(i).getName() + "\",\n" +
                    "            \"image_url\":\"" + result.getItems().get(i).getPhotos().get(0).getUrl() + "\",\n" +
                    "            \"subtitle\":\"" + result.getItems().get(i).getAddress() + "\",\n" +
                    "            \"default_action\": {\n" +
                    "              \"type\": \"web_url\",\n" +
                    "              \"url\": \"https://hotellook.com" + result.getItems().get(i).getLink() + "\",\n" +
                    "              \"messenger_extensions\": true,\n" +
                    "              \"webview_height_ratio\": \"tall\",\n" +
                    "              \"fallback_url\": \"https://hotellook.com" + result.getItems().get(i).getLink() + "\"\n" +
                    "            }      \n" +
                    "          }";

            if (i != result.getItems().size() && i != 10) {
                responseBody = responseBody + ",\n";
            } else {
                responseBody = responseBody + "\n";
            }
        }

        responseBody = responseBody +
                "        ]\n" +
                "      }\n" +
                "    }\n" +
                "  }\n" +
                "}";

        response.setMessage(getResponseBody());
        return response.getMessage();
    }


    public String getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }

}

